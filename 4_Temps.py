import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(input())  # the number of temperatures to analyse
max=5526
for i in input().split():
    # t: a temperature expressed as an integer ranging from -273 to 5526
    t = int(i)
    if (abs(t)<abs(max)):
        max=t
    elif (t==(-1)*max):
        max=abs(t)


# Write an answer using print
# To debug: print("Debug messages...", file=sys.stderr, flush=True)
if n==0:
    print(0)
else:
    print(max)
