import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int lx = in.nextInt(); // the X position of the light of power
        int ly = in.nextInt(); // the Y position of the light of power
        int tx = in.nextInt(); // Thor's starting X position
        int ty = in.nextInt(); // Thor's starting Y position

        // game loop
        while (true) {
            int remainingTurns = in.nextInt(); // The remaining amount of turns Thor can move. Do not remove this line.
            if(tx==lx){
                if(ty<ly){
                    System.out.println("S");
                    ty++;
                }
                else{
                    System.out.println("N");
                    ty--;
                }
            }
            else if(ty==ly){
                if(tx<lx){
                    System.out.println("E");
                    tx++;
                }
                else{
                    System.out.println("W");
                    tx--;
                }
            }
            else if(tx<lx){
                if(ty<ly){System.out.println("SE");tx++;ty++;}
                else if(ty>ly){System.out.println("NE");tx++;ty--;}
            } 
            else if(tx>lx){
                 if(ty<ly){System.out.println("SW");tx++;ty++;}
                else if(ty>ly){System.out.println("NW");tx++;ty--;}
            }
            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");


            // A single line providing the move to be made: N NE E SE S SW W or NW
        }
    }
}
